import pandas as pd
from sklearn.cluster import KMeans

data = pd.read_csv("../data/merged.csv")

# data[['context', 'gender', 'country', 'acct_age_weeks']]

###############################################################
# Correlation between acct age and time played
###############################################################

f = {'ms_played': ['sum'], 'acct_age_weeks': ['max']}
user_by_time_and_act_age = data.groupby(["user_id"], as_index=False).agg(f)
[['user_id', 'ms_played', 'acct_age_weeks']]
user_by_time_and_act_age.to_csv("../data/age_to_time.csv", index=False)

f = {'user_id': ['count']}
total_context_plays = data.groupby(["context"], as_index=False).agg(f)
total_sum_context_plays = total_context_plays.sum(axis=0)[1]
print "Probability of a song being played by a particular context in terms of percentage"
total_context_plays["percentage"] = total_sum_context_plays
total_context_plays["percentage"] = total_context_plays.user_id["count"] * 100 / total_context_plays["percentage"]
print total_context_plays[["context", "percentage"]].to_string(index=False)
print "\n This clearly shows that most of the songs are being heard via playlists."
print "Also search has a scope of improvement. Album, Artist and Collections are not that different"

print "\n Dividing this further for gender"
male = data[data.gender == "male"]
female = data[data.gender == "female"]
male_total_context_plays = male.groupby(["context"], as_index=False).agg(f)
female_total_context_plays = female.groupby(["context"], as_index=False).agg(f)

male_total_sum_context_plays = male_total_context_plays.sum(axis=0)[1]

male_total_context_plays["percentage"] = male_total_sum_context_plays
male_total_context_plays["percentage"] = \
    male_total_context_plays.user_id["count"] * 100 / male_total_context_plays["percentage"]
ml = male_total_context_plays[["context", "percentage"]]

female_total_sum_context_plays = female_total_context_plays.sum(axis=0)[1]
print "\n\nProbability of a song being played by a particular context in terms of percentage by males and females"
female_total_context_plays["percentage"] = female_total_sum_context_plays
female_total_context_plays["percentage"] = \
    female_total_context_plays.user_id["count"] * 100 / female_total_context_plays["percentage"]
fml = female_total_context_plays[["context", "percentage"]]

male_female_context_pctg = ml.merge(fml, on="context", suffixes=('_male', '_female'))
print male_female_context_pctg.to_string(index=False)

print "A few things we can say looking at this data is \n1. Males are almost twice more likely to " \
      "search than females" \
      "\n2. 'me' context is completely dominated by males" \
      "\n3. Females are more likely to play music via playlists"

###############################################################
# Correlation between product and gender
###############################################################
male_total_product_plays = male.groupby(["product"], as_index=False).agg(f)
female_total_product_plays = female.groupby(["product"], as_index=False).agg(f)

male_total_sum_product_plays = male_total_product_plays.sum(axis=0)[1]

male_total_product_plays["percentage"] = male_total_sum_product_plays
male_total_product_plays["percentage"] = \
    male_total_product_plays.user_id["count"] * 100 / male_total_product_plays["percentage"]
ml = male_total_product_plays[["product", "percentage"]]

female_total_sum_product_plays = female_total_product_plays.sum(axis=0)[1]
print "\n\nProbability of a song being played by a particular product in terms of percentage by males and females"
female_total_product_plays["percentage"] = female_total_sum_product_plays
female_total_product_plays["percentage"] = \
    female_total_product_plays.user_id["count"] * 100 / female_total_product_plays["percentage"]
fml = female_total_product_plays[["product", "percentage"]]

male_female_product_pctg = ml.merge(fml, on="product", suffixes=('_male', '_female'))
print male_female_product_pctg.to_string(index=False)


###############################################################
# Cluster users based on how frequently it is played
###############################################################

def distinct_count(df, col):
    f = {col: ['count']}
    tmp = df.groupby(col).agg(f)
    tmp_sum = tmp.sum(axis=0)[0]
    tmp["percentage"] = tmp_sum
    tmp["percentage"] = \
        tmp[col]["count"] * 100 / tmp["percentage"]
    tmp_ml = tmp[[col, "percentage"]]
    tmp_ml[col] = tmp_ml.index
    return tmp_ml


def print_cluster(zero, one, two, three, four, col):
    zero_age = distinct_count(zero, col)
    one_age = distinct_count(one, col)
    two_age = distinct_count(two, col)
    three_age = distinct_count(three, col)
    four_age = distinct_count(four, col)
    # dfs = [zero_age, one_age, two_age, three_age, four_age]
    # df_final = pd.merge(one_age, two_age, how="inner", on=col)
    # print df_final
    print zero_age
    print one_age
    print two_age
    print three_age
    print four_age


# f = {'track_id': ['count']}
# user_context = data.groupby(["user_id", "context"], as_index=False).agg(f)
# user_context["count"] = user_context.track_id["count"]
#
# user_context_pivot = user_context[['context', 'user_id', 'count']].pivot_table(index='user_id', columns='context',
#                                                                                values='count')
# user_context_pivot = user_context_pivot.fillna(0)
# user_context_pivot.to_csv("../data/users_by_context.csv")
data = pd.read_csv("../data/user_data_sample.csv")
users_by_context = pd.read_csv("../data/users_by_context.csv")
kmeans_model = KMeans(n_clusters=5, random_state=1).fit(users_by_context.iloc[:, 1:])
labels = kmeans_model.labels_
clusters = pd.crosstab(labels, users_by_context["user_id"])

clusters = clusters.transpose()
clusters['user_id'] = clusters.index
vals = pd.merge(clusters, data, on="user_id")
vals = pd.merge(vals, users_by_context, on="user_id")

# Can be iterated
zero = vals[vals[0] == 1]
one = vals[vals[1] == 1]
two = vals[vals[2] == 1]
three = vals[vals[3] == 1]
four = vals[vals[4] == 1]
# gender,age_range,country,acct_age_weeks,user_id
print_cluster(zero, one, two, three, four, 'age_range')
print_cluster(zero, one, two, three, four, 'gender')

print "\n"
print zero[["album", "app", "artist", "collection", "me", "playlist", "search", "unknown"]].sum()
print one[["album", "app", "artist", "collection", "me", "playlist", "search", "unknown"]].sum()
print two[["album", "app", "artist", "collection", "me", "playlist", "search", "unknown"]].sum()
print three[["album", "app", "artist", "collection", "me", "playlist", "search", "unknown"]].sum()
print four[["album", "app", "artist", "collection", "me", "playlist", "search", "unknown"]].sum()


