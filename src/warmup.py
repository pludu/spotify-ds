import pandas as pd

data = pd.read_csv("../data/merged.csv")

###############
# Basics
# Male vs Female
# 1. filter based on count of tracks
# 2. filter based on total time spent listening
###############


male = data[data.gender == "male"]
female = data[data.gender == "female"]

unique_male = len(pd.unique(male.user_id))
unique_female = len(pd.unique(female.user_id))

print "Total users by gender"
print "Female: "+str(unique_female)+" Male: "+str(unique_male)


# 1
male_track_count = len(male)
female_track_count = len(female)

print "\nCount of tracks played by"
print "Female: "+str(female_track_count)+" Male: "+str(male_track_count)
print "Average number of tracks played by gender"
print "Female: "+str(female_track_count/unique_female)+" Male: "+str(male_track_count/unique_male)
male_track_count = len(pd.unique(male.track_id))
female_track_count = len(pd.unique(female.track_id))
print "Unique number of tracks played by gender"
print "Female: "+str(female_track_count)+" Male: "+str(male_track_count)

# 2
male_ms_played = male.ms_played.sum()
female_ms_played = female.ms_played.sum()

print "\nTime(ms) spent on listening tracks by"
print "Female: "+str(female_ms_played)+" Male: "+str(male_ms_played)
print "Average time(ms) of tracks played by gender"
print "Female: "+str(female_ms_played/unique_female)+" Male: "+str(male_ms_played/unique_male)
top_listeners_grp = data.groupby(["user_id", "gender"], as_index=False)
top_listeners = top_listeners_grp[['ms_played']].sum().sort_values(by='ms_played', ascending=False)

print "\nTop listeners by gender"
print top_listeners[0:20]

print "\nBased of these statistics male and female users are not that different in their music listening behavior"



###############
# Advanced
# Male vs Female
###############
print "\n-----------------------------"
print "Moving to advances analysis"
print "-----------------------------"

common_trks = male.merge(female, how='inner', on='track_id')
unique_trks = len(pd.unique(data.track_id))
print "They may listen to similar number of tracks but the tracks they listen are overlapping only by: "
print str(len(pd.unique(common_trks.track_id))*100.0/unique_trks)+" %"
print "This gives a hint that male and female may be listening to different clusters of songs"

# print "\n which may be because of"

male_by_country = male.groupby("country", as_index=False).sum()
female_by_country = female.groupby("country", as_index=False).sum()
m = male_by_country.ix[:, 0:2]
f = female_by_country.ix[:, 0:2]
# country_overlap_outer = pd.merge(m, f, on="country", how="outer", suffixes=('_male', '_female'))
# print country_overlap_outer

print "\nwhich may also mean, males and females are coming from different countries in this data-sets\nalso: "

country_overlap_inner = pd.merge(m, f, on="country", how="inner", suffixes=('_male', '_female'))
row_iterator = country_overlap_inner.iterrows()
m_p_count = 0
f_p_count = 0
c_p_count = 0
for i, row in row_iterator:
    pctg = int(row['ms_played_male'])*100.0/(int(row['ms_played_male'])+int(row['ms_played_female']))
    # print("for country "+row['country']+" males played "+str(pctg)+" % of time of the total time")
    if pctg < 40:
        f_p_count += 1
    elif pctg > 60:
        m_p_count += 1
    else:
        c_p_count += 1

print "\nFor " + str(m_p_count) + " countries males dominate the listening time"
print "while, for " + str(f_p_count) + " countries females dominate the listening time"
print "although, there are " + str(c_p_count) + " gender neutral countries int terms of the total listening time"
print "\n\n\n-----------------------------"
print "Conclusion"
print "-----------------------------"
print "\nThis analysis gives us basic understanding that:" \
      "\n* Overall listening time of both genders do not differ to a great extent" \
      "\n* When looked at the common tracks played by both gender we found only 17% similarity" \
      "\n* This means males and females have different choices of songs" \
      "\n* When compared country wise, many countries showed skew towards a particular gender"\
      " in terms of total listening time"
print "\nWe will try to find these clusters in next part"


'''
Cluster based of track played count
'''