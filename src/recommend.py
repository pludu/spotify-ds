import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

data = pd.read_csv("../data/merged.csv")

f = {'context': ['count']}
user_tracks = data.groupby(["track_id", "user_id"], as_index=False).agg(f)
user_tracks["count"] = user_tracks.context["count"]

user_tracks_pivot = user_tracks[['track_id', 'user_id', 'count']].pivot('track_id', 'user_id')['count']
df_wide = user_tracks_pivot.fillna(0)
print df_wide

dists = cosine_similarity(df_wide)
# stuff the distance matrix into a dataframe so it's easier to operate on
dists = pd.DataFrame(dists, columns=df_wide.index)

# give the indicies (equivalent to rownames in R) the name of the product id
dists.index = dists.columns


def get_sims(products):
    p = dists[products].apply(lambda row: np.sum(row), axis=1)
    p = p.order(ascending=False)
    return p.index[p.index.isin(products) == False]


get_sims(["fff30b4de2db43e0b1b506bf530330e1", "ffe12ac37fe34853a504262423556984"])
